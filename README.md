# AnonVote

A lightweight anonymous vote system.

## Setup Step

0. Choose proper port in `index.js`.
1. Build Frontend  
   `cd Frontend && npm install && npm run build`
2. Start Backend  
   `npm install && node index.js`

## Note

The vote data use in-memory store, so it will disappear once backend shutdown.  
You could backup your own data by saving the result from http://YOUR_SERVER/results periodically.
