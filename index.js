const { createHash } = require('crypto');
const express = require('express');
const app = express()
const port = 3001;

app.use(express.static('Frontend/dist'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

function sleep(msec) {
  return new Promise(resolve => setTimeout(resolve, msec));
}

let key2idx = new Map();
let results = [];

app.get('/results', (req, res) => {
  return res.send(results);
});
app.post('/results', async (req, res) => {
  const key = req.body.name;
  if (typeof key === 'undefined' || key === '') {
    return res.status(400).send({'status': 'Bad'});
  }
  let idx = key2idx.get(key);
  if (typeof idx === 'undefined') {
    key2idx.set(key, results.length);
    idx = results.length;
    results.push({
      count: 0,
      name: key,
      nameHash: createHash('sha256').update(key, 'utf8').digest('hex')
    })
  }
  results[idx].count += 1;
  console.log(`${key} voted.`);
  await sleep(600);
  return res.send({'status': 'OK'});
});

app.listen(port, () => {
  console.log(`Listening at PORT=${port}`);
});
